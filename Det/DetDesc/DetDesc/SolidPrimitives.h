#ifndef     __DETDESC_SOLID_SOLIDPRIMITIVES_H__
#define     __DETDESC_SOLID_SOLIDPRIMITIVES_H__



#include "DetDesc/SolidBox.h" 
#include "DetDesc/SolidCons.h" 
#include "DetDesc/SolidPolycone.h" 
#include "DetDesc/SolidTubs.h" 
#include "DetDesc/SolidTrd.h" 
#include "DetDesc/SolidTrap.h" 
#include "DetDesc/SolidSphere.h"



#endif  //  __DETDESC_SOLID_SOLIDPRIMITIVES_H__
