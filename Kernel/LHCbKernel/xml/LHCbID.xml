<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE gdd SYSTEM "gdd.dtd">
<gdd>
  <package name="LHCbKernel">

    <class
       name   = "LHCbID"
       desc   = "LHCb wide channel identifier"
       author = "Marco Cattaneo"
       final  = "TRUE"
       defaultdestructor = "FALSE"
       virtual= "FALSE">

       <import name="Kernel/VeloChannelID"/>
       <import name="Kernel/STChannelID"/>
       <import name="Kernel/OTChannelID"/>
       <import name="Kernel/RichSmartID"/>
       <import name="Kernel/CaloCellID"/>
       <import name="Kernel/MuonTileID"/>
       <import name="Kernel/VPChannelID"/>
       <import name="Kernel/FTChannelID"/>
       <import name="Kernel/HCCellID"/>

       &StlVector;

       <enum
          desc   = "types of sub-detector channel ID"
          name   = "channelIDtype"
          value  = "Velo=1, TT, IT, OT, Rich, Calo, Muon, VP, FT=10, UT , HC"
          access = 'PUBLIC'
       />


       <attribute
          desc = "the internal representation" name="lhcbID"
          type = "bitfield"
          setMeth = "FALSE" >
            <bitfield desc='the ID bits (to recreate the channelID)' length='28' name='ID' setMeth='TRUE' getMeth='FALSE' />
            <bitfield desc='the LHCb detector type bits' length='4' name='detectorType' />
       </attribute>

       <method
         type    = 'bool'
         name    = 'operator=='
         argList = 'const LHCbID chanID'
         const   = 'TRUE'
         desc    = 'comparison equality'>
         <code>
  return (this->lhcbID() == chanID.lhcbID());
         </code>
       </method>

       <method
         type    = 'bool'
         name    = 'operator&lt;'
         argList = 'const LHCbID chanID'
         const   = 'TRUE'
         desc    = 'comparison ordering'>
         <code>
  return  ( m_lhcbID &lt; chanID.m_lhcbID ) ;
         </code>
       </method>

       <constructor
         desc     = "Constructor from unsigned int "
         explicit = "TRUE"
         argList  = "unsigned int theID"
         initList = "m_lhcbID(theID)"
       />

       <constructor
         desc  = "Constructor from VeloChannelID " >
          <arg const="TRUE" name="chanID" type="VeloChannelID" />
          <code>
  m_lhcbID = (channelIDtype::Velo &lt;&lt; detectorTypeBits) + chanID;
          </code>
       </constructor>

       <method
         type  = "bool"
         name  = "isVelo"
         const = 'TRUE'
         desc  = "return true if this is a Velo identifier">
         <code>
  return channelIDtype::Velo == detectorType();
         </code>
       </method>

       <method
         type  = "bool"
         name  = "isVeloR"
         const = 'TRUE'
         desc  = "return true if this is a Velo R identifier">
         <code>
  return isVelo() &amp;&amp; veloID().isRType();
         </code>
       </method>


       <method
         type  = "bool"
         name  = "isVeloPhi"
         const = 'TRUE'
         desc  = "return true if this is a Velo Phi identifier">
         <code>
  return isVelo() &amp;&amp; veloID().isPhiType();
         </code>
       </method>

        <method
         type  = "bool"
         name  = "isVeloPileUp"
         const = 'TRUE'
         desc  = "return true if this is a Velo Pile up identifier">
         <code>
  return isVelo() &amp;&amp; veloID().isPileUp();
         </code>
       </method>

       <method
         type  = "LHCb::VeloChannelID"
         name  = "veloID"
         const = 'TRUE'
         desc  = "return the VeloChannelID">
	 <code>
  return ( isVelo() ? LHCb::VeloChannelID(m_lhcbID &amp; IDMask) : LHCb::VeloChannelID(0xF0000000));
         </code>
       </method>

       <constructor
         desc  = "Constructor from VPChannelID, VP corresponding to pixel solution for upgrade" >
          <arg const="TRUE" name="chanID" type="VPChannelID" />
          <code>
  m_lhcbID = (channelIDtype::VP &lt;&lt; detectorTypeBits) + chanID;
          </code>
       </constructor>

       <method
         type  = "bool"
         name  = "isVP"
         const = 'TRUE'
         desc  = "return true if this is a VP identifier">
         <code>
  return channelIDtype::VP == detectorType();
         </code>
       </method>

       <method
         type  = "LHCb::VPChannelID"
         name  = "vpID"
         const = 'TRUE'
         desc  = "return the VPChannelID">
	 <code>
  return LHCb::VPChannelID( isVP()? m_lhcbID &amp; IDMask : 0xF0000000 );
         </code>
       </method>

       <constructor
         desc     = "Constructor from STChannelID " >
          <arg const="TRUE" name="chanID" type="STChannelID" />
          <code>
  unsigned int stType = channelIDtype::TT;
  if ( chanID.isIT() ) stType = channelIDtype::IT;
  if ( chanID.isUT() ) stType = channelIDtype::UT;
  m_lhcbID = (stType &lt;&lt; detectorTypeBits) + chanID;
        </code>
      </constructor>

       <method
         type  = "bool"
         name  = "isTT"
         const = 'TRUE'
         desc  = "return true if this is a TT Silicon Tracker identifier">
         <code>
  return channelIDtype::TT == detectorType();
         </code>
       </method>

       <method
         type  = "bool"
         name  = "isUT"
         const = 'TRUE'
         desc  = "return true if this is a UT Silicon Tracker identifier">
         <code>
  return channelIDtype::UT == detectorType();
         </code>
       </method>

       <method
         type  = "bool"
         name  = "isIT"
         const = 'TRUE'
         desc  = "return true if this is a IT Silicon Tracker identifier">
         <code>
  return channelIDtype::IT == detectorType();
         </code>
       </method>

       <method
         type  = "bool"
         name  = "isST"
         const = 'TRUE'
         desc  = "return true if this is a Silicon Tracker identifier (i.e. TT, IT or UT)">
         <code>
  return isTT() || isUT() || isIT();
         </code>
       </method>

       <method
         type  = "LHCb::STChannelID"
         name  = "stID"
         const = 'TRUE'
         desc  = "return the STChannelID">
	 <code>
  return LHCb::STChannelID( isST() ? m_lhcbID &amp; IDMask : 0xF0000000);
         </code>
       </method>

       <constructor
         desc     = "Constructor from OTChannelID" >
        <arg const="TRUE" name="chanID" type="OTChannelID" />
        <code>
  m_lhcbID = (channelIDtype::OT &lt;&lt; detectorTypeBits) + chanID;
        </code>
       </constructor>

       <method
         type  = "bool"
         name  = "isOT"
         const = 'TRUE'
         desc  = "return true if this is a Outer Tracker identifier">
         <code>
  return (channelIDtype::OT == detectorType());
         </code>
       </method>

       <method
         type  = "LHCb::OTChannelID"
         name  = "otID"
         const = 'TRUE'
         desc="return the OTChannelID">
	 <code>
  return LHCb::OTChannelID( isOT() ? m_lhcbID &amp; IDMask : 0xF0000000 );
         </code>
       </method>

       <method
         type = "bool"
         name = "isBitOn"
         desc = "Checks if a given bit is set"
         virtual = "FALSE"
         access  = "PRIVATE"
         const   = "TRUE">
         <arg type = "unsigned int" name = "pos" const = "TRUE"/>
         <code>
           return ( 0 != ( lhcbID() &amp; (1&lt;&lt;pos) ) );
         </code>
       </method>

       <method
         type = "void"
         name = "setBit"
         desc = "Sets the given bit to the given value"
         virtual = "FALSE"
         access  = "PRIVATE"
         const   = "FALSE">
         <arg type = "unsigned int" name = "pos"   const = "TRUE"/>
         <arg type = "unsigned int" name = "value" const = "TRUE"/>
         <code>
           m_lhcbID |= value&lt;&lt;pos;
         </code>
       </method>

       <constructor
         desc     = "Constructor from RichSmartID"
         initList = "m_lhcbID(0)" >
         <arg const="TRUE" name="chanID" type="RichSmartID" />
         <code>
  // Set the type to be RICH
  setDetectorType( channelIDtype::Rich );
  // Save the data bits
  setID( chanID.dataBitsOnly() );
  // Save the MaPMT/HPD flag in bit 27
  setBit( 27, chanID.idType() );
  // Set the validity bits
  setBit( 26, chanID.pixelDataAreValid() );
         </code>
       </constructor>

       <method
         type  = 'bool'
         name  = 'isRich'
         const = 'TRUE'
         desc  = 'return true if this is a Rich identifier'>
         <code>
  return channelIDtype::Rich == detectorType();
         </code>
       </method>

       <method
         type  = "LHCb::RichSmartID"
         name  = "richID"
         const = 'TRUE'
         desc  = "return the richSmartID">
	 <code>
  // Create the RichSMartID data bits
  LHCb::RichSmartID::KeyType data( isRich() ? (m_lhcbID&amp;IDMask) : 0 );
  // Create a temporary RichSmartID
  LHCb::RichSmartID tmpid ( data );
  // Retrieve the MaPMT/HPD flag
  if ( isRich() ) { tmpid.setIDType( (LHCb:: RichSmartID::IDType)isBitOn(27) ); }
  // Object to return, with RICH and panel fields set
  LHCb::RichSmartID id( tmpid.rich(),       tmpid.panel(),
                        tmpid.pdNumInCol(), tmpid.pdCol(),
                        tmpid.idType() );
  // Set pixels fields
  if ( isBitOn(26) )
  {
     id.setPixelRow    ( tmpid.pixelRow()    );
     id.setPixelCol    ( tmpid.pixelCol()    );
     if ( tmpid.idType() == LHCb::RichSmartID::HPDID )
     {
       id.setPixelSubRow ( tmpid.pixelSubRow() );
     }
  }
  // return
  return id;
         </code>
       </method>

       <constructor
          desc     = "Constructor from CaloCellID"
          initList = "m_lhcbID(0)" >
          <arg const="TRUE" name="chanID" type="CaloCellID" />
         <code>
  setDetectorType( channelIDtype::Calo );
  setID( chanID.all() );
         </code>
       </constructor>

       <method
         type  = "bool"
         name  = "isCalo"
         const = 'TRUE'
         desc  = "return true if this is a Calo identifier">
         <code>
  return channelIDtype::Calo == detectorType();
         </code>
       </method>

       <method
         type  = "LHCb::CaloCellID"
         name  = "caloID"
         const = 'TRUE'
         desc  = "return the CaloCellID">
         <code>
  return isCalo() ? CaloCellID(m_lhcbID &amp; IDMask) : CaloCellID(0xF0000000);
         </code>
       </method>

       <constructor
          desc     = "Constructor from MuonTileID"
          initList = "m_lhcbID(0)" >
          <arg const="TRUE" name="chanID" type="MuonTileID" />
         <code>
  setDetectorType( channelIDtype::Muon );
  setID( int(chanID) );
         </code>
       </constructor>

       <method
         type  = "bool"
         name  = "isMuon"
         const = 'TRUE'
         desc  = "return true if this is a Muon identifier">
         <code>
  return channelIDtype::Muon == detectorType();
         </code>
       </method>

       <method
         type  = "LHCb::MuonTileID"
         name  = "muonID"
         const = 'TRUE'
         desc  = "return the MuonTileID">
         <code>
  return isMuon() ? MuonTileID(m_lhcbID &amp; IDMask) : MuonTileID(0xF0000000);
         </code>
       </method>

       <constructor
         desc     = "Constructor from FTChannelID" >
        <arg const="TRUE" name="chanID" type="FTChannelID" />
        <code>
  m_lhcbID = ((unsigned int)channelIDtype::FT &lt;&lt; detectorTypeBits) + chanID;
        </code>
       </constructor>

       <method
         type  = "bool"
         name  = "isFT"
         const = 'TRUE'
         desc  = "return true if this is a Fibre Tracker identifier">
         <code>
  return channelIDtype::FT == detectorType();
         </code>
       </method>

       <method
         type  = "LHCb::FTChannelID"
         name  = "ftID"
         const = 'TRUE'
         desc="return the FTChannelID">
	 <code>
  return (isFT() ? LHCb::FTChannelID(m_lhcbID &amp; IDMask) : LHCb::FTChannelID(0xF0000000 ));
         </code>
       </method>

       <constructor
         desc  = "Constructor from HCCellID, HC standing for Herschel" >
          <arg const="TRUE" name="cellID" type="HCCellID" />
          <code>
  m_lhcbID = (channelIDtype::HC &lt;&lt; detectorTypeBits) + cellID;
          </code>
       </constructor>

       <method
         type  = "bool"
         name  = "isHC"
         const = 'TRUE'
         desc  = "return true if this is a HC identifier">
         <code>
  return channelIDtype::HC == detectorType();
         </code>
       </method>

       <method
         type  = "LHCb::HCCellID"
         name  = "hcID"
         const = 'TRUE'
         desc  = "return the HCCellID">
	 <code>
  return ( isHC() ? LHCb::HCCellID(m_lhcbID &amp; IDMask) : LHCb::HCCellID(0xF0000000));
         </code>
       </method>

       <method
         type  = "bool"
         name  = "checkDetectorType"
         argList = "unsigned int channelIDType"
         desc  = "Check the LHCbID sub-detector channel ID type identifier"
         const = "TRUE">
         <code>
  return ( channelIDType == detectorType() );
         </code>
       </method>

      <method
        type    = "unsigned int"
        name    = "channelID"
        const   = "TRUE"
        desc    = "General ID: returns detector ID = internal unsigned int">
        <code>
  switch ( detectorType() ) {
    case channelIDtype::VP:    return vpID().channelID();
    case channelIDtype::Velo:  return veloID().channelID();
    case channelIDtype::TT:    //C++17 [[fall-through]]
    case channelIDtype::UT:    //C++17 [[fall-through]]
    case channelIDtype::IT:    return stID().channelID();
    case channelIDtype::OT:    return otID().channelID();
    case channelIDtype::Rich:  return richID().key();
    case channelIDtype::Calo:  //C++17 [[ fall-through ]]
    case channelIDtype::Muon:  return m_lhcbID &amp; IDMask;
    case channelIDtype::FT:    return ftID().channelID();
    case channelIDtype::HC:    return hcID().cellID();
    default:    return 0;
  }
        </code>
      </method>

      <method
          name    = "fillStream"
          desc    = "Print this LHCbID in a human readable way"
          type    = "std::ostream&amp;"
          virtual = "FALSE"
          const   = "TRUE">
          <arg
            type = "std::ostream"
            name = "s"/>
      </method>

    </class>
  </package>
</gdd>
